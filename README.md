# Python Package Template

This repository contains a [Copier](https://copier.readthedocs.io/en/stable/) project template.

## What is it?

This template help you set up a git repo for developing, testing and deploying your Python package.

### What is setup for you locally

* `src/` folder that will contain the source code (inside the folder named after your package)
* `test/` folder that will contain your tests
* `pyproject.toml` setup to build your source code as a python package, with all specified dependencies etc.
* `requirements-dev.txt` with all the dependencies needed for development
* `requirements-test.txt` with all the dependencies needed for testing (mostly meant for CI/CD, developers should use `requirements-dev.txt`)
* `.pre-commit-config.yaml` with all the pre-commit hooks that will be run before each commit (`pre-commit-hooks`, `black`, `mypy`, `refurb`, `ruff`)
* `README.md` with a minimal template, and few instructions for you to follow to complete the package setup
* `CHANGELOG.md` that you can use to keep track of the changes in your package
* A python virtualenv in the folder `venv` with [`pre-commit`](https://pre-commit.com/) already installed

### What is setup for CI/CD (GitLab only)

* Tests are run automatically on all commits
* Code style is checked automatically on all commits (using pre-commit)
* On tags of the form `vX.Y.Z`, the package is built and pushed to a repository of your choice (look into the generated README for details)

## Quickstart

1. This template assumes that you are using [pyenv](https://github.com/pyenv/pyenv) to manage python versions.
   We strongly recommend that you install it and install the version of python you plan to use for your project before proceeding further.
2. Generate your project from this template using Copier.
   You can find instructions on how to do so [here](https://repos.eon-cds.de/data-science-lib/templates/copier/wiki/usage/).
3. Follow the instructions in the generated README, under the "TODO" section, to complete the package setup.
