import os
import subprocess
from pathlib import Path
from tempfile import TemporaryDirectory
from unittest import TestCase

import copier
from parameterized import parameterized
from questionary import ValidationError


class TestInputValidity(TestCase):
    commit_sha = subprocess.run(
        ["git", "rev-parse", "HEAD"], stdout=subprocess.PIPE, text=True
    ).stdout.strip()

    def setUp(self) -> None:
        self.tmpdir = TemporaryDirectory()
        os.chdir(self.tmpdir.name)
        os.system("git init")

    def tearDown(self) -> None:
        self.tmpdir.cleanup()

    @parameterized.expand(
        [
            ("empty string", ""),
            ("whitespace", " "),
            ("start with digit", "1test"),
            ("start with underscore", "_test"),
            ("start with dash", "-test"),
            ("contain empty space", "test package"),
            ("contain underscore", "test_package"),
        ]
    )
    def test_invalid_package_names_raise_exception(self, _: str, pkg_name: str):
        with self.assertRaises(ValidationError):
            self.use_copier(custom_answers={"package_name": pkg_name})

    @parameterized.expand(
        [
            ("only letters", "abc"),
            ("letters and dashes", "a-b-c"),
            ("letters, digits, and dashes", "a1-b2-c3"),
        ]
    )
    def test_valid_package_names_do_not_raise_exception(self, _: str, pkg_name: str):
        self.use_copier(custom_answers={"package_name": pkg_name})

    @parameterized.expand(
        [
            ("empty string", ""),
            ("whitespace", " "),
            ("start with whitespace", " test description"),
            ("end with whitespace", "test description "),
        ]
    )
    def test_invalid_package_short_description_raise_exception(
        self, _: str, pkg_short_description: str
    ):
        with self.assertRaises(ValidationError):
            self.use_copier(
                custom_answers={"package_short_description": pkg_short_description}
            )

    @parameterized.expand(
        [
            ("regular text", "This is a description"),
            ("text starting with a digit", "1 of many options for a description"),
        ]
    )
    def test_valid_package_short_description_do_not_raise_exception(
        self, _: str, pkg_short_description: str
    ):
        self.use_copier(
            custom_answers={"package_short_description": pkg_short_description}
        )

    @parameterized.expand(
        [
            ("empty string", ""),
            ("one whitespace", " "),
            ("ony one digit", "3"),
            ("only major and minor", "3.10"),
            ("- as separator", "3-10-9"),
            ("no separator", "3109"),
        ]
    )
    def test_invalid_python_version_raise_exception(self, _: str, py_version: str):
        with self.assertRaises(ValidationError):
            self.use_copier(custom_answers={"python_version": py_version})

    @parameterized.expand(
        [
            ("v3", "3.10.11"),
            ("v2", "2.7.0"),
        ]
    )
    def test_valid_python_version_do_not_raise_exception(self, _: str, py_version: str):
        self.use_copier(custom_answers={"python_version": py_version})

    @parameterized.expand(
        [
            ("empty string", ""),
            ("one whitespace", " "),
            ("ony one digit", "3"),
            ("full version", "3.10.11"),
            ("two versions comma-separated", "3.10.11,3.11.0"),
            ("only comparison operator", ">="),
            ("two comparison oeprators comma-separated", ">=,=="),
            ("comparison operator followed by letters", ">A"),
        ]
    )
    def test_invalid_python_requires_raise_exception(self, _: str, py_requires: str):
        with self.assertRaises(ValidationError):
            self.use_copier(custom_answers={"python_requires": py_requires})

    @parameterized.expand(
        [
            ("greater or equal to 3", ">=3"),
            ("major is 3", ">=3,<4"),
            ("grater or equal to 3.8", ">=3.8"),
            ("grater or equal to 3.8 and less than 4", ">=3.8,<4"),
        ]
    )
    def test_valid_python_requires_do_not_raise_exception(
        self, _: str, py_requires: str
    ):
        self.use_copier(custom_answers={"python_requires": py_requires})

    @parameterized.expand(
        [
            ("empty string", ""),
            ("start with whitespace", " Test Name"),
            ("end with whitespace", "Test Name "),
        ]
    )
    def test_invalid_author_name_raise_exception(self, _: str, author_name: str):
        with self.assertRaises(ValidationError):
            self.use_copier(custom_answers={"author_name": author_name})

    @parameterized.expand(
        [
            ("name surname", "Name Surname"),
            ("only name", "Name"),
            ("full name with commas", "Surname, Name"),
        ]
    )
    def test_valid_author_name_do_not_raise_exception(self, _: str, author_name: str):
        self.use_copier(custom_answers={"author_name": author_name})

    @parameterized.expand(
        [
            ("empty string", ""),
            ("start with whitespace", " test@example.com"),
            ("end with whitespace", "test@example.com "),
            ("no at", "test.example.com"),
            ("no domain", "test@example"),
            ("no name", "@example.com"),
        ]
    )
    def test_invalid_author_email_raise_exception(self, _: str, author_email: str):
        with self.assertRaises(ValidationError):
            self.use_copier(custom_answers={"author_email": author_email})

    @parameterized.expand(
        [
            ("name surname", "name.surname@eon.com"),
            ("name only", "name@eon.com"),
        ]
    )
    def test_valid_author_email_do_not_raise_exception(self, _: str, author_email: str):
        self.use_copier(custom_answers={"author_email": author_email})

    def use_copier(self, custom_answers: dict = {}):
        copier.run_copy(
            self.get_template_path().as_posix(),
            self.tmpdir.name,
            data=self.build_answers(custom_answers=custom_answers),
            vcs_ref=self.commit_sha,
        )

    @staticmethod
    def get_template_path() -> Path:
        return Path(__file__).parent.parent.parent

    @staticmethod
    def build_answers(custom_answers: dict) -> dict:
        answers = {
            "package_name": "test-package-name",
            "package_short_description": "Short description of test package",
            "python_version": "3.10.9",
            "python_requires": ">3.8,<3.50",
            "author_name": "Test Author",
            "author_email": "test@email.com",
            "license": "MIT",
            "test_framework": "unittest",
        }
        answers.update(custom_answers)
        return answers
