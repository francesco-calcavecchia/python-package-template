import os
import subprocess
from pathlib import Path
from tempfile import TemporaryDirectory
from unittest import TestCase

import copier


class BaseTestFileContent:
    package_name = "test-package"
    package_short_description = "test description"
    python_version = "3.11.4"
    python_requires = ">=3.8"
    author_name = "Test Author"
    author_email = "test@me.com"
    package_license = "Test License"
    test_framework = "pytest"
    commit_sha = subprocess.run(
        ["git", "rev-parse", "HEAD"], stdout=subprocess.PIPE, text=True
    ).stdout.strip()
    tmpdir: TemporaryDirectory

    @classmethod
    def setUpClass(cls) -> None:
        cls.tmpdir = TemporaryDirectory()
        os.chdir(cls.tmpdir.name)
        os.system("git init")
        cls.use_copier()

    @classmethod
    def tearDownClass(cls) -> None:
        cls.tmpdir.cleanup()

    @classmethod
    def use_copier(cls):
        copier.run_copy(
            cls.get_template_path().as_posix(),
            cls.tmpdir.name,
            data=cls.get_example_answers(),
            vcs_ref=cls.commit_sha,
        )

    @staticmethod
    def get_template_path() -> Path:
        return Path(__file__).parent.parent.parent

    @classmethod
    def get_example_answers(cls) -> dict:
        return {
            "package_name": cls.package_name,
            "package_short_description": cls.package_short_description,
            "python_version": cls.python_version,
            "python_requires": cls.python_requires,
            "author_name": cls.author_name,
            "author_email": cls.author_email,
            "license": cls.package_license,
            "test_framework": cls.test_framework,
        }

    def test_destination_folder_exits(self):
        assert Path(self.tmpdir.name).exists()

    def test_gitlab_ci_file_exists(self):
        assert (Path(self.tmpdir.name) / ".gitlab-ci.yml").exists()

    def test_python_version_file_exists(self):
        assert (Path(self.tmpdir.name) / ".python-version").exists()

    def test_pyproject_file_exists(self):
        assert (Path(self.tmpdir.name) / "pyproject.toml").exists()

    def test_readme_exists(self):
        assert (Path(self.tmpdir.name) / "README.md").exists()

    def test_requirements_exists(self):
        assert (Path(self.tmpdir.name) / "requirements-dev.txt").exists()

    def test_changelog_exists(self):
        assert (Path(self.tmpdir.name) / "CHANGELOG.md").exists()

    def test_precommit_config_exists(self):
        assert (Path(self.tmpdir.name) / ".pre-commit-config.yaml").exists()

    def test_src_folder_exists(self):
        assert (Path(self.tmpdir.name) / "src").exists()

    def test_test_folder_exists(self):
        assert (Path(self.tmpdir.name) / "test").exists()

    def test_mac_setup_exists(self):
        assert (Path(self.tmpdir.name) / "dev_env_setup/setup_linux_mac.sh").exists()

    def test_windows_setup_exists(self):
        assert (Path(self.tmpdir.name) / "dev_env_setup/setup_windows.cmd").exists()

    def test_answer_file_exists(self):
        assert (Path(self.tmpdir.name) / ".copier-answers.yml").exists()

    def test_package_name_is_used_in_pyproject_file(self):
        file_content = (Path(self.tmpdir.name) / "pyproject.toml").read_text()
        assert f'name = "{self.package_name}"' in file_content

    def test_test_framework_in_requirements(self):
        file_content = (Path(self.tmpdir.name) / "requirements-test.txt").read_text()
        assert self.test_framework in file_content

    def test_test_framework_in_gitlab_ci(self):
        file_content = (Path(self.tmpdir.name) / ".gitlab-ci.yml").read_text()
        assert self.test_framework in file_content


class TestFileContentWithDefaults(BaseTestFileContent, TestCase):
    pass


class TestFileContentWithUnittest(BaseTestFileContent, TestCase):
    test_framework = "unittest"

    def test_test_framework_in_requirements(self):
        file_content = (Path(self.tmpdir.name) / "requirements-test.txt").read_text()
        assert "pytest" not in file_content
