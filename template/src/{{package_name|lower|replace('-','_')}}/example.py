def f(a: int, b: int) -> int:
    return (a + 1) * (b - 1)
