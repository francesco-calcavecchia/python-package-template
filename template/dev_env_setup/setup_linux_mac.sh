echo "Running setup script..."
if command -v python &>/dev/null; then
    python -m venv venv
    echo "Python virtual environment created with python"
elif command -v python3 &>/dev/null; then
    python3 -m venv venv
    echo "Python virtual environment created with python3"
else
    echo "Error: Python is not installed or is not available under the following executable names: [python, python3]. Please install Python before running this script."
    exit 1
fi
. venv/bin/activate
python -m pip install -U pip wheel setuptools pre-commit
python -m pip install -r requirements-dev.txt
pre-commit install
echo "Setup script finished."
