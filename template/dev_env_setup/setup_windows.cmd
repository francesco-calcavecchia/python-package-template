@echo off
echo Running setup script...

:: Check if Python is available as "python"
where python >nul 2>nul
if %errorlevel% == 0 (
    python -m venv venv
    echo Python virtual environment created with python
) else (
    :: Check if Python is available as "python3"
    where python3 >nul 2>nul
    if %errorlevel% == 0 (
        python3 -m venv venv
        echo Python virtual environment created with python3
    ) else (
        echo Error: Python is not installed or is not available under the following executable names: [python, python3]. Please install Python before running this script.
        exit /b 1
    )
)

:: Activate the virtual environment
call venv\Scripts\activate.bat

:: Upgrade pip and install packages
python -m pip install -U pip wheel setuptools pre-commit
python -m pip install -r requirements-dev.txt
pre-commit install

:: Optional: Display a message when the script is finished
echo Setup script finished.
